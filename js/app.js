//USTREAM Front-End Developer test task
//by zoli@digitsdestroy.com



//main app, immediately invoked
var app = (function () {

	//code structure based on MVC pattern
	var Model, View, Controller;



	//defining the Model
	Model = {

		//array for storing currently used elements
		elements: [],

		//a model for our elements, with properties for helping element validation
		elementModel: {
			id: 'number',
			thumbnail: /^(ftp|http|https):\/\/[^ "]+$/,
			title: /^\s*\S.*$/,
			description: /^\s*\S.*$/,
			date: 'number',
			type: /^(channel|video)$/,
			isLive: 'boolean'
		},

		//the sort parameter and order of the list, default is by 'id', ascending
		sortBy: 'id',

		sortAscending: true,

		//validate passed elements and replace 'elements' array if new set contains at least one valid element
		//if there are no valid elements in the new set, keep old 'elements' contents
		validateElements: function validateElements(els) {
			var self = this,
				success = false,
				newElements = [];	

			//helper function for asserting a variable's type
			function isType(a,type) {
				return (typeof a === type) ? true : false;
			}

			//function for validating an element as defined by 'elementModel'
			function validate(el) {
				var valid = true;

				for (var key in self.elementModel) {

					if (self.elementModel.hasOwnProperty(key)) {

						//these properties are validated by checking type
						//the element's property should be the same type as defined in 'elementModel'
						if (key == 'id' || key == 'date' || key == 'isLive') {
							valid &= isType(el[key],self.elementModel[key]);
						}

						//these properties are validated by string regex comparisons as defined in 'elementModel'
						else {
							valid &= self.elementModel[key].test(el[key]);
						}
					}				
				}

				return valid;
			}

			//loop over passed elements and validate their content
			for (var i = 0, l = els.length; i < l; i++)
			{
				//store valid elements in 'newElements' array
				if (validate(els[i]))
				{
					newElements.push(els[i]);
				}
			}

			//there is at least one 'good' element from the current fetch
			if (newElements.length > 0)
			{
				//replace current elements with the new set, set success flag to true
				this.elements = newElements;
				success = true;
			}

			//if there is no 'good' element in the new set 'elements' remains unchainged
			//return with false 'success' flag
			return success;
		},

		//method for updating localStorage after a successful update of the Model
		updateStorage: function updateStorage() {

			//get watch later collection string from localStorage
			var wlater = localStorage.getItem('watchLater');

			if (wlater) {

				//cut off leading and trailing commas from watchLater string to be able to split into a clean array
				wlater = wlater.substring(1, wlater.length - 1);

				//make array out of ids in watch later string
				wl = wlater.split(',');

				//reset watch later string
				wlater = ',';

				var j = this.elements.length;

				//check all elements in Model, if 'id' matches an 'id' in the watch later array, keep it
				//model elements can arbitrarily change after an update, all ids must be checked for a match
				while (j--)
				{
					if (wl.indexOf( this.elements[j].id + '' ) !== -1) {

						//rebuild string from matched ids
						wlater += this.elements[j].id + ',';
					}
				}

				//if new watch later string did not get emptied out, store it in localStorage
				if (wlater != ',') {
					localStorage.setItem('watchLater',wlater);

				//if new watch later string got emptied out, remove it from localStorage
				} else {
					localStorage.removeItem('watchLater');
				}
			}
		},

		//if passed id is in localStorage, remove it, if not, add it
		toggleWatchLater: function toggleWatchLater(id) {

			//get watch later collection string from localStorage
			var wlater = localStorage.getItem('watchLater') || ',';

			//remove current id from string
			var newlater = wlater.replace(',' + id + ',' , ',');

			//if current id was not in the string, add it to the end
			if (newlater == wlater) {
				newlater += id + ',';
			}

			//if new watch later string did not get emptied out, store it in localStorage
			if (newlater != ',') {
				localStorage.setItem('watchLater', newlater);

			//if new watch later string got emptied out, remove it from localStorage
			} else {
				localStorage.removeItem('watchLater');
			}
		},

		//fetch new elements from jsonp api and update Model
		updateModel: function updateModel(callback) {

			$.ajax({
				url: 'http://50.18.153.2/app.php',
				jsonp: 'callback',
				jsonpCallback: 'tztztz',
				dataType: 'jsonp',
				//jsonp loading errors can only be detected with a timeout!
				timeout: 4500,
				success: function(response) {

					//check returned elements and call the callback if the returned elements validate
					if (response.elements && Model.validateElements(response.elements)) {
						Model.updateStorage();
						callback();
					}
					//in case of an invalid response, notify the 'Controller'. Callback is not called and the update dies
					else {
						Controller.notifyController('invalid API response');
					}
				},

				//in case of an error (timeout...), notify the 'Controller'. Callback is not called and the update dies
				error: function() {
					Controller.notifyController('API timeout');
				}
			});
		},

		//method for sorting the elements in Model
		sortModel: function sortModel(by,callback) {
			var self = this;

			//store the currently selected sorting option (this.sortBy survives through updates)
			if (by) {
				this.sortBy = by;				
			}

			//the compare function for our array sort, return changes according to the current sort order
			//the key to sort by is stored in the sortBy property
			function compare(a,b) {
				if (a[self.sortBy] > b[self.sortBy])
					return self.sortAscending ? 1 : -1;
				if (a[self.sortBy] < b[self.sortBy])
					return self.sortAscending ? -1 : 1;
				return 0;
			}

			//sort the array
			this.elements.sort(compare);

			//call the callback and pass in the elements
			callback(Model.elements);
		},

		//toggle the sort order
		changeSortOrder: function changeSortOrder(callback) {
			this.sortAscending ? this.sortAscending = false : this.sortAscending = true;
			callback();
		},
	};



	//defining the View
	View = {

		//List View
		List: {

			//html template for rendering our list dynamically, {{mustached}} data fields will be replaced by string replaces
			itemTemplate
			:	'<li data-itemtype="{{filter}}">'
			+		'<div class="mediaitem-image-wrap{{watchlater}}"><a data-id="{{id}}"><img src="{{thumbnail}}"></a></div>'
			+		'<h3>{{title}}</h3>'
			+		'<div class="details">ID: {{idd}}, Date: {{date}}, {{isLive}} {{type}}</div>'
			+		'<div class="description">{{description}}</div>'
			+	'</li>',

			//render the passed elements' list View html and attach it to the DOM
			renderListView: function renderListView(els) {

				//helper function to get a formatted date string from epoch time format
				function formatDate(epoch) {
					var d = new Date(epoch * 1000);
					return d.getFullYear()
						+ '.' + ('0'+d.getMonth()).substr(-2,2)
						+ '.' + ('0'+d.getDate()).substr(-2,2)
						+ ' ' + ('0'+d.getHours()).substr(-2,2)
						+ ':' + ('0'+d.getMinutes()).substr(-2,2);
				}

				//helper function to check if passed 'id' is in localStorage, for 'watch later' functionality
				function inStorage(id) {

					//get watch later collection string from localStorage
					var wlater = localStorage.getItem('watchLater') || '';

					//regex for current search 'id'
					var searchRegexp = new RegExp(',' + id + ','); 

					//'id' found in watch later string
					if (wlater.search(searchRegexp) != -1) {
						return true;

					//'id' not found
					} else {
						return false;

					}
				}

				//helper function to return a display string according to the element's 'type' and 'isLive' properties
				function notVideo(a) {
					if (a.type !== 'video')
					{
						return a.isLive == true ? 'Live' : 'Offline'
					}
					else return '';
				}

				//build the html fragment string
				var fragment = '';

				if (els && els.length > 0) {

					//loop through the passed elements, adding the html template for every element
					//and replacing the data fields with string replace
					for (var i = 0, l = els.length ; i < l; i++) {

						var template = View.List.itemTemplate;
						template = template.replace('{{filter}}', els[i].isLive == true ? 'live' : (els[i].type == 'channel' ? 'offline' : 'video'));
						template = template.replace('{{watchlater}}', inStorage(els[i].id) ? ' watch-later' : '');
						template = template.replace('{{id}}', els[i].id);
						template = template.replace('{{thumbnail}}', els[i].thumbnail);
						template = template.replace('{{title}}', els[i].title);
						template = template.replace('{{idd}}', els[i].id);
						template = template.replace('{{date}}', formatDate(els[i].date));
						template = template.replace('{{isLive}}', notVideo(els[i]));
						template = template.replace('{{type}}', els[i].type.charAt(0).toUpperCase() + els[i].type.slice(1));
						template = template.replace('{{description}}', els[i].description.length > 200 ? els[i].description.substr(0,200) + '...' : els[i].description);
						fragment += template;

					}

					//replace the media list section with the new fragment in the DOM
					var $medialist = $('#medialist');
					$medialist.children().remove();
					$medialist.append($(fragment));

					//when done updating the DOM, remove the fade from the list, and hide error messages
					View.List.fadeListView(false);
					View.Error.dismissErrorMessage();
				}
			},

			//method to add filtering to our list
			//pure CSS filtering, with parent - child and data-attribute selector rules
			filterListView: function filterListView(by) {

				var $medialist = $('#medialist');
				$medialist.removeClass();
				
				if (by) {
					$medialist.addClass('filter-' + by);
				}
			},

			//fade the list view by adding CSS class to to parent node
			//change cursor to waiting
			fadeListView: function fadeList(fade) {

				if (fade) {
					$(document.documentElement).addClass('cursor-wait');
					$('#medialist').addClass('fade');
				} else {
					$(document.documentElement).removeClass('cursor-wait');
					$('#medialist').removeClass('fade');
				}
			},

		},

		Settings: {

			//toggle the sort order link between 'Ascending' and 'Descending'
			flipSortOrderButton: function flipSortOrderButton() {
				var $order = $('a#order');
				$order.text().substr(0,1) == 'A' ? $order.html('Descending <b>&#9660;</b>') : $order.html('Ascending <b>&#9650;</b>');
			},

			//set the navigation links to a clicked state by adding a CSS class
			setClicked: function setClicked(who) {

				var $who = $(who);
				var a = $who.attr('id').substr(0,4);

				if (a == 'sort') {
					$('.sort-link').removeClass('clicked');
				} else if (a == 'poll') {
					$('.poll-link').removeClass('clicked');
				} else {
					$('.button').removeClass('clicked');
				}

				$who.addClass('clicked');
			},

			//attach click event handlers for all clickable links
			bindClickHandlers: function bindClickHandlers() {

				//click handlers for list elements
				$('#medialist').on('click', 'a', function(event) {
					event.preventDefault();
					var $img = $(event.target);
					$img.parent().parent().toggleClass('watch-later');
					Controller.flipWatchLater($img.parent().attr('data-id'));
				});

				//click handlers for navigation links
				$('#link-container').on('click', 'a', function(event) {
					event.preventDefault();
					var t = event.target,
						linkType = t.id.split('-');

					if(t.id !== 'order') {
						View.Settings.setClicked(t);
					}

					switch(linkType[0]) {
						case 'sort':
							Controller.sortList(linkType[1]);
							break;
						case 'filter':
							Controller.filterList(linkType[1]);
							break;
						case 'poll':
							Controller.startPolling(+linkType[1] * 1000);
							break;
						case 'order':
							View.Settings.flipSortOrderButton();
							Controller.toggleSortOrder();
							break;
						default:
						// do nothing
					}
				});

				//click handler for error dismiss button
				$('a#error-dismiss').click(function(){View.Error.dismissErrorMessage();});
			}

		},

		Error: {

			//show the error message in an overlay View
			showErrorMessage: function showErrorMessage(msg) {
				$('#error-message').text('A server error occured while refreshing the list: ' + msg);
				$('#error-container').show();
			},

			//dismiss the error message
			dismissErrorMessage: function dismissErrorMessage() {
				$('#error-container').hide();
			}

		}

	};




	//defining the Controller
	Controller = {

		//update polling interval
		timerID: null,

		//interface for notifying the Controller
		notifyController: function notifyController(message) {

			//when an error message is sent, tell the View to show the message and stop fading the list
			View.Error.showErrorMessage(message);
			View.List.fadeListView(false);
		},

		//sort the list
		sortList: function sortList(sortBy) {

			//call the Model to sort the Model by sortBy, then render the View with the callback
			Model.sortModel(sortBy,View.List.renderListView);		
		},

		//toggle the sort order of the list
		toggleSortOrder: function toggleSortOrder() {

			//call the Model to change the sort order of the Model, then sort the Model with a callback
			Model.changeSortOrder(Controller.sortList);
		},

		//filter the list
		filterList: function filterList(filterBy) {

			//no need to call the model, View just changes CSS styles on the parent html element
			//elements are filtered by the corresponding CSS rules
			View.List.filterListView(filterBy);
		},

		//update the list
		updateList: function updateList() {

			View.List.fadeListView(true);

			//call the Model to update the Model, then sort the list with a callback
			Model.updateModel(Controller.sortList);
		},

		//start a timer for updating the list according to the passed argument
		startPolling: function startPolling(delay) {

			//clear any running timer
			if (this.timerID !== null)
			{
				clearInterval(this.timerID);
			}

			//call the update function
			Controller.updateList();

			//set the timer for repeadetly calling the list update method
			this.timerID = setInterval(Controller.updateList, delay);
		},

		//toggle the 'watch later' setting of the passed id
		flipWatchLater: function flipWatchLater(id) {
			Model.toggleWatchLater(id);
		},

		//Controller initialization function
		init: function init() {

			//bind the click handlers
			View.Settings.bindClickHandlers();

			//start polling in intervals
			Controller.startPolling(10000);
		}
	};



	//return initialization method and 'test' object referencing the Model, View and Controller for unit testing purposes
	return {
		init: Controller.init,
		test: {
			model: Model,
			view: View,
			controller: Controller
		}
	};

}());