//reference 'public' properties of the app object
var Model = app.test.model,
	View = app.test.view,
	Controller = app.test.controller,

	//for binding and testing click event initiated functionality and html view renders
	dummies = $('<div id="dummies" style="display:none"></div>').appendTo(document.documentElement),

	//test elements for validation
	goodElement = {
		id: 1,
		thumbnail: 'http://www.placecage.com/200/300',
		title: 'non-empty string',
		description: 'non-empty string',
		date: 1373096178,
		type: 'channel',
		isLive: true
	},
	badElement = JSON.parse(JSON.stringify(goodElement)),
	newElements;





//
// Jasmine tests
//
describe("Application", function() {
	it("should be running", function() {
		expect( app ).toBeDefined();
	});

	describe("Controller", function() {

		beforeEach(function() {
			dummies.append('<div id="link-container">'
								+ '<a id="filter-live"></a>'
								+ '<a id="order"></a>'
								+ '<a id="sort-id"></a>'
								+ '</div>'
			);
			View.Settings.bindClickHandlers();
		});

		afterEach(function() {
			dummies.children().remove();
		});

		it("should start polling after initialization", function() {
			spyOn(Controller, "startPolling");
			Controller.init();
			expect(Controller.startPolling).toHaveBeenCalled();
		});

		it("should update the list and set a timer when starting polling", function() {
			spyOn(Controller, "updateList");
			Controller.startPolling();
			expect(Controller.updateList).toHaveBeenCalled();
			expect(Controller.timerID).not.toBeNull();
		});

		it("should tell the Model to update, when starting the update", function() {
			spyOn(Model, "updateModel");
			Controller.updateList();
			expect(Model.updateModel).toHaveBeenCalled();
		});

		it("should tell the View to fade the list, when starting the update", function() {
			spyOn(View.List, "fadeListView");
			Controller.updateList();
			expect(View.List.fadeListView).toHaveBeenCalled();
		});

		it("should tell the View to filter the list when a filtering link was clicked", function() {
			spyOn(View.List, "filterListView");
			$('a#filter-live').click();
			expect(View.List.filterListView).toHaveBeenCalled();
		});

		it("should tell the Model to change the sort order, when the sorting order link was clicked", function() {
			spyOn(Model, "changeSortOrder");
			$('a#order').click();
			expect(Model.changeSortOrder).toHaveBeenCalled();
		});

		it("should tell the Model to sort the list, when the sorting order link was clicked", function() {
			spyOn(Model, "sortModel");
			$('a#sort-id').click();
			expect(Model.sortModel).toHaveBeenCalled();
		});

		it("should tell the View to show the error message and un-fade the list, when Controller is notified", function() {
			spyOn(View.Error, "showErrorMessage");
			spyOn(View.List, "fadeListView");
			Controller.notifyController();
			expect(View.Error.showErrorMessage).toHaveBeenCalled();
			expect(View.List.fadeListView).toHaveBeenCalled();
		});

		it("should tell the Model to toggle the \"Watch Later\" status of the given id", function() {
			spyOn(Model, "toggleWatchLater");
			Controller.flipWatchLater();
			expect(Model.toggleWatchLater).toHaveBeenCalled();
		});

	});


	describe("Model", function() {

		beforeEach(function() {
			Model.elements = [];
		});

		it("validation: should replace currently stored elements if passed new good elements", function() {
			newElements = [goodElement];
			var ret = Model.validateElements(newElements);
			expect(ret).toBe(true);
			expect(Model.elements).toEqual(newElements);
		});

		it("validation: should keep currently stored elements if passed new bad elements", function() {
			badElement.id = 'this should be a number';
			newElements = [badElement];
			var ret = Model.validateElements(newElements);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual(newElements);
		});

		it("validation: should replace currently stored elements with only the good new elements if passed a mixture of good and bad elements", function() {
			badElement.id = 'this should be a number';
			newElements = [goodElement,badElement,goodElement];
			var onlyTheGood = [goodElement,goodElement];
			var ret = Model.validateElements(newElements);
			expect(ret).toBe(true);
			expect(Model.elements).toEqual(onlyTheGood);
		});

		it("element validation: id should be a number", function() {
			badElement.id = 'this should be a number';
			newElements = [badElement];
			var ret = Model.validateElements([badElement]);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual([badElement]);
		});

		it("element validation: thumbnail should be a url", function() {
			badElement.thumbnail = 'not-a-url';
			newElements = [badElement];
			var ret = Model.validateElements([badElement]);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual([badElement]);
		});

		it("element validation: title should be a non-empty string", function() {
			badElement.title = '   ';
			newElements = [badElement];
			var ret = Model.validateElements([badElement]);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual([badElement]);
		});

		it("element validation: description should be a non-empty string", function() {
			badElement.description = '   ';
			newElements = [badElement];
			var ret = Model.validateElements([badElement]);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual([badElement]);
		});

		it("element validation: date should be a number (posix)", function() {
			badElement.date = 'monday';
			newElements = [badElement];
			var ret = Model.validateElements([badElement]);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual([badElement]);
		});

		it("element validation: type should be 'channel' or 'video' ", function() {
			badElement.type = 'not-channel';
			newElements = [badElement];
			var ret = Model.validateElements([badElement]);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual([badElement]);
		});

		it("element validation: isLive should be boolean", function() {
			badElement.isLive = 'tru';
			newElements = [badElement];
			var ret = Model.validateElements([badElement]);
			expect(ret).toBe(false);
			expect(Model.elements).not.toEqual([badElement]);
		});
		it("local storage should be updated when the model updates: remove item from storage, if not in update", function() {
			Model.elements = [goodElement];
			localStorage.setItem('watchLater',',2,3,');
			Model.updateStorage();
			expect(localStorage.getItem('watchLater')).toEqual(null);
		});

		it("local storage should be updated when the model updates: keep item in storage, if in update", function() {
			Model.elements = [goodElement];
			var gi = goodElement.id;
			localStorage.setItem('watchLater',',' + gi + ',');
			Model.updateStorage();
			expect(localStorage.getItem('watchLater')).toEqual(',' + gi + ',');
		});

		it("watch later item should be added to local storage if not already in it", function() {
			localStorage.removeItem('watchLater');
			Model.toggleWatchLater(99);
			expect(localStorage.getItem('watchLater')).toEqual(',99,');
		});

		it("watch later item should be removed from local storage if already in it", function() {
			localStorage.setItem('watchLater',',99,');
			Model.toggleWatchLater(99);
			expect(localStorage.getItem('watchLater')).toEqual(null);
		});

		it("should update the model with an ajax request, executing the callback function and updating the local storage on success", function () {
			spyOn(Model, 'updateStorage');
			spyOn($, 'ajax').andCallFake(function(options) {
				var fakedata = {elements : [goodElement]}
				options.success(fakedata);
			});
			var callback = jasmine.createSpy();
			Model.updateModel(callback);
			expect(callback).toHaveBeenCalled();
			expect(Model.updateStorage).toHaveBeenCalled();
		});

		it("should notify the controller on update failure", function () {
			spyOn(Controller, 'notifyController');
			spyOn($, 'ajax').andCallFake(function(options) {
				options.error();
			});
			var callback = jasmine.createSpy();
			Model.updateModel(callback);
			expect(Controller.notifyController).toHaveBeenCalled();
		});

		it("should sort the elements in the correct order and call the callback when finished", function () {
			var Element1 = JSON.parse(JSON.stringify(goodElement)),
				Element2 = JSON.parse(JSON.stringify(goodElement)),
				Element3 = JSON.parse(JSON.stringify(goodElement));
			Element2.id = 2;
			Element3.id = 3;
			Model.elements = [Element2,Element1,Element3];
			var sortedOrder = [Element1,Element2,Element3];
			var callback = jasmine.createSpy();
			Model.sortModel('id',callback);
			expect(callback).toHaveBeenCalled();
			expect(Model.elements).toEqual(sortedOrder);
		});

		it("should toggle the sort order and the call the callback", function () {
			var previousOrder = Model.sortAscending;
			var callback = jasmine.createSpy();
			Model.changeSortOrder(callback);
			expect(Model.sortAscending).not.toEqual(previousOrder);
			expect(callback).toHaveBeenCalled();
		});
	});


	describe("View", function() {

		beforeEach(function() {
			dummies.append('<div id="link-container">'
								+ '<a id="filter-live"></a>'
								+ '<a id="order">Ascending</a>'
								+ '<a id="sort-id"></a>'
								+ '</div>'
			);
			dummies.append('<div id="medialist"></div>');
			dummies.append('<div id="error-message"></div>');
			View.Settings.bindClickHandlers();
		});

		afterEach(function() {
			dummies.children().remove();
		});

		it("should render the view of the list from the provided model elements", function() {
			var renderElements = [goodElement,goodElement];
			View.List.renderListView(renderElements);
			expect($('#medialist').children.length).toEqual(renderElements.length);
		});

		it("should filter the view of the list by changing the style class of the parent node", function() {
			View.List.filterListView('description');
			expect($('#medialist').hasClass('filter-description')).toEqual(true);
		});

		it("should fade the view of the list and change the cursor to \"wait\" to indicate loading", function() {
			View.List.fadeListView(true);
			expect($('#medialist').hasClass('fade')).toEqual(true);
			expect($(document.documentElement).hasClass('cursor-wait')).toEqual(true);
		});

		it("should toggle the sort order link between ascending/descending", function() {
			View.Settings.flipSortOrderButton();
			expect($('#order').html().substr(0,10)).toEqual('Descending');
		});

		it("should show the passed error message in an overlay", function() {
			View.Error.showErrorMessage('magic');
			expect($('#error-message').text()).toEqual('A server error occured while refreshing the list: magic');
		});
	});

});